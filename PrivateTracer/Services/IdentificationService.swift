//
//  IdentificationService.swift
//  PrivateTracer
//
//  Created by Sumeru Chatterjee.
//

import Foundation
import RxSwift

protocol IdentificationService {

    func getUserIdentity() -> Single<UserIdentity>

}
