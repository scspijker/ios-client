//
//  ServiceInjection.swift
//  PrivateTracer
//
//  Created by Sumeru Chatterjee.
//

import Foundation

class ServiceInjection {

    static let shared: ServiceInjection = ServiceInjection()

    lazy var identificationService: IdentificationService = { return IdentificationServiceImplementation() }()

    static func provideIdentificationService() -> IdentificationService {
        return shared.identificationService
    }
}
