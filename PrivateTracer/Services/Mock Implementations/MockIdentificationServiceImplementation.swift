//
//  IdentificationServiceImplementation.swift
//  PrivateTracer
//
//  Created by Sumeru Chatterjee.
//

import RxSwift

class MockIdentificationServiceImplementation: IdentificationService {

    static let instance = MockIdentificationServiceImplementation()

    func getUserIdentity() -> Single<UserIdentity> {
        return Single.just(UserIdentity(identifier: "MOCK-MOCK-MOCK"))
    }

}
