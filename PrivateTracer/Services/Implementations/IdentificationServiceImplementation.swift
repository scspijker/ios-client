//
//  IdentificationServiceImplementation.swift
//  PrivateTracer
//
//  Created by Sumeru Chatterjee.
//

import RxSwift

class IdentificationServiceImplementation: IdentificationService {

    func getUserIdentity() -> Single<UserIdentity> {
        return Single.just(UserIdentity(identifier: "123-123-1234"))
    }

}
