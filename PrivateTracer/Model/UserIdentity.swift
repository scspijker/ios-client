//
//  UserIdentity.swift
//  PrivateTracer
//
//  Created by Sumeru Chatterjee.
//

import Foundation

struct UserIdentity {

    let identifier: String
}
