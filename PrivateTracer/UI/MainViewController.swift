//
//  MainViewController.swift
//  PrivateTracer
//
//  Created by Sumeru Chatterjee on 16/04/2020.
//

import UIKit
import FontAwesome_swift

class MainViewController: UIViewController {
    
    @IBOutlet weak var historyButton: UIButton!
    @IBOutlet weak var iamInfectedButton: UIButton!
    @IBOutlet weak var settingsButton: UIButton!
    @IBOutlet weak var infoLabel: UILabel!
    @IBOutlet weak var mainButton: UIButton!
    @IBOutlet weak var infoButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        historyButton.backgroundColor = UIColor(red: 0.91, green: 0.91, blue: 0.91, alpha: 0.30)
        historyButton.layer.cornerRadius = 10.0
        historyButton.clipsToBounds = true
        historyButton.setImage(UIImage.fontAwesomeIcon(name: .history, style: .solid, textColor: UIColor.black, size: CGSize(width: 30, height: 30)), for: .normal)
        historyButton.tintColor = .white
        historyButton.setTitle("", for: .normal)
        
        iamInfectedButton.backgroundColor = UIColor(red: 0.91, green: 0.91, blue: 0.91, alpha: 0.30)
        iamInfectedButton.layer.cornerRadius = 10.0
        iamInfectedButton.clipsToBounds = true
        iamInfectedButton.setTitleColor(.white, for: .normal)
        iamInfectedButton.titleLabel?.font = UIFont.boldSystemFont(ofSize: 15)
        iamInfectedButton.setTitle(Strings.App.Mainview.Button.infected, for: .normal)
        
        settingsButton.backgroundColor = UIColor(red: 0.91, green: 0.91, blue: 0.91, alpha: 0.30)
        settingsButton.layer.cornerRadius = 10.0
        settingsButton.clipsToBounds = true
        settingsButton.setImage(UIImage.fontAwesomeIcon(name: .cog, style: .solid, textColor: UIColor.black, size: CGSize(width: 30, height: 30)), for: .normal)
        settingsButton.tintColor = .white
        settingsButton.setTitle("", for: .normal)
        
        infoLabel.textColor = .white
        infoLabel.font = UIFont.systemFont(ofSize: 15)
        infoLabel.text = Strings.App.Mainview.Label.info
        
        mainButton.backgroundColor = .clear
        mainButton.tintColor = .white
        
        infoButton.backgroundColor = UIColor(red: 0.91, green: 0.91, blue: 0.91, alpha: 0.30)
        infoButton.layer.cornerRadius = 10.0
        infoButton.clipsToBounds = true
        infoButton.setTitleColor(.white, for: .normal)
        infoButton.titleLabel?.font = UIFont.boldSystemFont(ofSize: 15)
        infoButton.setTitle(Strings.App.Mainview.Button.info, for: .normal)
    }
    
    /*
     // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
