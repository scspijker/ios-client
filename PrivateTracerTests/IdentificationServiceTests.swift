//
//  PrivateTracerTests.swift
//  IdentificationServiceTests
//
//  Created by Sumeru Chatterjee.
//

import XCTest
import RxSwift

@testable import PrivateTracer

class IdentificationServiceTests: XCTestCase {

    var identificationService: IdentificationService?
    var userIdentity: UserIdentity?
    private let disposeBag = DisposeBag()
    
    override func setUp() {
        identificationService = ServiceInjection.provideIdentificationService()
    }
    
    func testIdentificationService() {
        let expectation = self.expectation(description: "UserIdentity")
        
        identificationService?.getUserIdentity().subscribe(onSuccess: { [weak self] userIdentity in
            self?.userIdentity = userIdentity
            expectation.fulfill()
            }, onError: { error in
                XCTFail("Error \(error)")
        }).disposed(by: self.disposeBag)
        
        waitForExpectations(timeout: 1, handler: nil)
        XCTAssert(userIdentity?.identifier.count ?? 0 > 0)
    }
    
}
